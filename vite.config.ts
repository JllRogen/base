import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

export default defineConfig(({ command, mode }) => {
  console.log('command:', command)
  return {
    plugins: [
      vue(), // vue解析
      vueJsx(), // 支持vue的jsx写法
    ],

    define: {
      // 自定义全局替换方式
      'process.env.params': JSON.stringify('rogen'),
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      preserveSymlinks: true, // 可以让npm link失效
    },
    css: {
      modules: {},
      postcss: {
        plugins: [
          require('tailwindcss'), //
          require('autoprefixer'),
        ],
      },
      preprocessorOptions: {
        scss: {
          additionalData: `
          $injectedColor: orange;
          $pColor: #348334;
          `,
        },
      },
    },
    json: {
      namedExports: true,
    },
    // esbuild: {
    //   jsxFactory: 'h',
    //   jsxFragment: 'Fragment',
    // },
    server: {
      port: 3000,
      strictPort: true,
    },
    build: {
      manifest: true,
    },
  }
})
