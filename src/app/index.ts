import '../styles/index.scss'
import { createApp } from 'vue'
import App from './app.vue'
import router from '../router'

export function craeteBaseApp() {
  const app = createApp(App)
  app.use(router)
  app.mount('#app')
  return app
}
