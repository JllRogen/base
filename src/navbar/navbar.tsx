import style from './index.module.scss'
import { defineComponent } from 'vue'
const btns = [
  {
    label: 'app1',
    onClick() {},
  },
  {
    label: 'demo',
    onClick() {},
  },
]

export default defineComponent({
  setup() {
    const btnNodes = btns.map((btn) => {
      return <el-button onClick={btn.onClick}>{btn.label}</el-button>
    })
    return () => {
      return (
        <div class={style.box}>
          <div class={style.inner}>{btnNodes}</div>
        </div>
      )
    }
  },
})
