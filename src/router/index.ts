import { createRouter, createWebHistory } from 'vue-router'

import { route as homeRoute } from '../pages/home'
import { route as aboutRoute } from '../pages/about'

const routes = [homeRoute, aboutRoute]

const router = createRouter({
  history: createWebHistory(),
  routes,
})
export default router
